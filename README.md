# CleanArchirecure.ShoppingCart

A repo to demonstrate and play with clean architecture and clean code practices and results

## Why this project?

I wanted for some time now, to be able to send a clear message as to why *clean code* and *clean arch* are important, so I went through the trouble of setting this up to enable me to share this knowledge with everyone.
Checkout the 

## Setting it all up

The app is built with .net Core, then either build the app with VS 2017 and the build command or, from the .sln file root folder and the CLI command:
>dotnet build

Then, go to package manager in VS 2017 and run:
>Update-Database

That generates the db from the EF migrations packed in this solution

You should be ready to run this sln, remember the starting project should be **CleanArchitecture.ShoppingCart**

## Exploring functionalities

Either go through the trouble of  running and executing the UI of this sln
**OR**
Just review and run the UTs! 
*(they're there for a reason, other than testing :-P)*