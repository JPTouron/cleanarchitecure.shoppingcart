﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace CleanArchitecture.Domain.Models.Tests
{
    [TestClass]
    public class PurchaseOrderTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Create_IfCustomerNameIsInvalid_ThenThrowsException()
        {
            var po = new PurchaseOrder("");
        }

        [TestMethod]
        public void Create_IfCustomerNameIsValid_ThenSucceeds()
        {
            var po = new PurchaseOrder("john doe");

            Assert.AreEqual("john doe", po.CustomerName);
            Assert.AreEqual(DateTime.Now.ToShortDateString(), po.PurchaseDate.ToShortDateString());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Create_IfCustomerNameIsWhiteSpace_ThenThrowsException()
        {
            var po = new PurchaseOrder(" ");
        }
    }
}