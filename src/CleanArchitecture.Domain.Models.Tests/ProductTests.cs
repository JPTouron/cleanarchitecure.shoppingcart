﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace CleanArchitecture.Domain.Models.Tests
{
    [TestClass]
    public class ProductTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Create_IfDescriptionIsEmpty_ThenThrowsException()
        {
            var p = new Product(1, "name", "", 0.5m, 4);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Create_IfDescriptionIsWhiteSpace_ThenThrowsException()
        {
            var p = new Product(1, "", " ", 0.5m, 4);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void Create_IfIdIsInvalid_ThenThrowsException()
        {
            var p = new Product(-3, "name", "desc", 0.5m, 4);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void Create_IfIdIsZero_ThenThrowsException()
        {
            var p = new Product(0, "name", "desc", 0.5m, 4);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Create_IfNameIsEmpty_ThenThrowsException()
        {
            var p = new Product(1, "", "desc", 0.5m, 4);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Create_IfNameIsWhiteSpace_ThenThrowsException()
        {
            var p = new Product(1, " ", "desc", 0.5m, 4);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void Create_IfPriceIsInvalid_ThenThrowsException()
        {
            var p = new Product(1, "name", "desc", -0.5m, 4);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void Create_IfPriceIsZero_ThenThrowsException()
        {
            var p = new Product(1, "name", "desc", 0, 4);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void Create_IfremainingStockIsInvalid_ThenThrowsException()
        {
            var p = new Product(1, "name", "desc", 0.5m, -1);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void Create_IfRemainingStockIsInvalid_ThenThrowsException()
        {
            var p = new Product(1, "name", "desc", 0.5m, -4);
        }
    }
}