﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace CleanArchitecture.Domain.Models.Tests
{
    [TestClass]
    public class ShoppingCartTests
    {
        private ShoppingCart sc;

        [TestMethod]
        public void AddProductToCart_WhenDifferentProductsExistInCart_ThenWeGetDifferentProductsFromCart()
        {
            var cartId = sc.Id;
            var productA = new Product(4, "Docker for dummies ", "Book about docker", 15.6m, 100);
            var productB = new Product(2, "TypeScript in Action", "Book about TypeScript", 32.27m, 100);
            var expectedQuantityFirst = 2;
            var expectedQuantitySecond = 5;
            var cartProdIdFirst = Guid.NewGuid();
            var cartProdIdSecond = Guid.NewGuid();
            var expectedProductFirst = new CartProduct(cartProdIdFirst, cartId, productA, expectedQuantityFirst);
            var expectedProductSecond = new CartProduct(cartProdIdSecond, cartId, productB, expectedQuantitySecond);
            var expectedProductsInCart = 2;

            sc.AddOrUpdateProductInCart(expectedProductFirst);
            sc.AddOrUpdateProductInCart(expectedProductSecond);

            var productInCart = sc.GetProductsInCart();

            Assert.AreEqual(expectedProductsInCart, sc.GetProductsInCart().Count);

            Assert.AreEqual(expectedProductFirst.Id, productInCart[0].Id);
            Assert.AreEqual(expectedProductSecond.Id, productInCart[1].Id);
            Assert.AreEqual(cartId, productInCart[0].ShoppingCartId);
            Assert.AreEqual(cartId, productInCart[1].ShoppingCartId);
        }

        [TestMethod]
        public void AddProductToCart_WhenExecutedAndNoSameProductExistInCart_ThenAProductIsAddedtoTheShoppingCart()
        {
            var cartProdId = Guid.NewGuid();
            var cartId = sc.Id;
            var product = new Product(4, "Docker for dummies ", "Book about docker", 15.6m, 100);
            var expectedProductsInCart = 1;
            var expectedQuantity = 2;

            var expectedProduct = new CartProduct(cartProdId, cartId, product, expectedQuantity);

            sc.AddOrUpdateProductInCart(expectedProduct);

            var productInCart = sc.GetProductsInCart().Single();

            Assert.AreEqual(expectedProductsInCart, sc.GetProductsInCart().Count);

            Assert.AreEqual(expectedProduct.Id, productInCart.Id);
            Assert.AreEqual(expectedProduct.ProductId, productInCart.ProductId);
            Assert.AreEqual(expectedProduct.Name, productInCart.Name);
            Assert.AreEqual(expectedProduct.Description, productInCart.Description);
            Assert.AreEqual(expectedProduct.Price, productInCart.Price);
            Assert.AreEqual(expectedProduct.Quantity, productInCart.Quantity);
            Assert.AreEqual(expectedProduct.ShoppingCartId, sc.Id);

            CollectionAssert.AllItemsAreNotNull(sc.GetProductsInCart().ToList());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void AddProductToCart_WhenProductIsNull_ThenThrowsException()
        {
            CartProduct product = null;

            sc.AddOrUpdateProductInCart(product);
        }

        [TestMethod]
        public void AddProductToCart_WhenSameProductExistInCart_ThenOldProductIsReplacedWithNew()
        {
            var cartId = sc.Id;
            var product = new Product(4, "Docker for dummies ", "Book about docker", 15.6m, 100);
            var expectedQuantityFirst = 2;
            var expectedQuantitySecond = 5;
            var cartProdIdFirst = Guid.NewGuid();
            var cartProdIdSecond = Guid.NewGuid();
            var expectedProductFirst = new CartProduct(cartProdIdFirst, cartId, product, expectedQuantityFirst);
            var expectedProductSecond = new CartProduct(cartProdIdSecond, cartId, product, expectedQuantitySecond);
            var expectedProductsInCart = 1;

            sc.AddOrUpdateProductInCart(expectedProductFirst);
            sc.AddOrUpdateProductInCart(expectedProductSecond);

            var productInCart = sc.GetProductsInCart().Single();

            Assert.AreEqual(expectedProductsInCart, sc.GetProductsInCart().Count);

            Assert.AreNotEqual(expectedProductFirst.Id, productInCart.Id);
            Assert.AreEqual(expectedProductSecond.Id, productInCart.Id);
            Assert.AreEqual(expectedProductSecond.Quantity, productInCart.Quantity);
            Assert.AreEqual(expectedProductSecond.ShoppingCartId, sc.Id);
        }

        [TestInitialize]
        public void Init()
        {
            sc = new ShoppingCart(Guid.NewGuid());
        }

        [TestMethod]
        public void WhenCreated_ThenShoppingCartHasGuidIdProvisinedByConstructorAndListOfProductsIsInitialized()
        {
            var expectedId = Guid.NewGuid();

            var cart = new ShoppingCart(expectedId);

            Assert.AreEqual(expectedId, cart.Id);
            Assert.IsNotNull(cart.GetProductsInCart());
        }
    }
}