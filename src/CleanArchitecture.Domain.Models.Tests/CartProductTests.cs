﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace CleanArchitecture.Domain.Models.Tests
{
    [TestClass]
    public class CartProductTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Create_IfIdIsEmpty_ThenThrowsException()
        {
            var p = new Product(1, "name", "desc", 0.5m, 4);

            var cp = new CartProduct(Guid.Empty, Guid.NewGuid(), p, 4);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Create_IfProductIsNull_ThenThrowsException()
        {
            Product p = null;

            var cp = new CartProduct(Guid.NewGuid(), Guid.NewGuid(), p, 4);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void Create_IfProductStockIsLowerThanCartProductQuantity_ThenThrowsException()
        {
            var p = new Product(1, "name", "desc", 0.5m, 3);

            var cp = new CartProduct(Guid.NewGuid(), Guid.NewGuid(), p, 4);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void Create_IfQuantityIsZero_ThenThrowsException()
        {
            var p = new Product(1, "name", "desc", 0.5m, 4);

            var cp = new CartProduct(Guid.NewGuid(), Guid.NewGuid(), p, 0);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Create_IfShoppingCartIdIsEmpty_ThenThrowsException()
        {
            var p = new Product(1, "name", "desc", 0.5m, 4);

            var cp = new CartProduct(Guid.NewGuid(), Guid.Empty, p, 4);
        }

        [TestMethod]
        public void Create_Succeeds()
        {
            var p = new Product(1, "name", "desc", 0.5m, 4);

            var cp = new CartProduct(Guid.NewGuid(), Guid.NewGuid(), p, 4);

            Assert.AreEqual(4, cp.Quantity);
        }

        [TestMethod]
        public void UpdateQuantity_Whenexecuted_ThenQuantityGetsUpdated()
        {
            var p = new Product(1, "name", "desc", 0.5m, 4);

            var cp = new CartProduct(Guid.NewGuid(), Guid.NewGuid(), p, 4);

            cp.UpdateQuantity(5);

            Assert.AreEqual(5, cp.Quantity);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void UpdateQuantity_WhenQuantityIsInvalid_ThenThrowsException()
        {
            var p = new Product(1, "name", "desc", 0.5m, 4);

            var cp = new CartProduct(Guid.NewGuid(), Guid.NewGuid(), p, 4);

            cp.UpdateQuantity(0);
        }
    }
}