import { Component, Inject, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-fetch-products',
  templateUrl: './fetch-products.component.html'
})
export class FetchProductsComponent implements OnInit {
  public products: Product[];
  private httpClient: HttpClient;
  private baseUrl: string;

  ngOnInit() {


  }
  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {

    this.httpClient = http;
    this.baseUrl = baseUrl;

    this.getAll();

  }

  public getAll() {
    this.httpClient.get<Product[]>(this.baseUrl + 'api/Products/GetAll').subscribe(result => {
      this.products = result;
      console.log(result[0].remainingStock);

    }, error => console.error(error));

  }

  public checkoutCart(custName: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    this.httpClient.post(this.baseUrl + 'api/Products/checkoutCart', JSON.stringify(custName), httpOptions).subscribe(result => {
      this.getAll();
    });

  }

  public addToCart(product: Product) {

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    this.httpClient.post<Product>(this.baseUrl + 'api/Products/AddToCart', product, httpOptions).subscribe(error => console.error(error));
  }
}

interface CheckoutData {
  customerName: string;
}

interface Product {
  name: string;
  description: string;
  price: number;
  remainingStock: number;
  productId: number;
  quantityToBuy: number;
  shoppingCartId: string;
}
