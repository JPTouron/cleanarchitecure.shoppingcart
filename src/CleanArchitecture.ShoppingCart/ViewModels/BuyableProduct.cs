﻿using System;

namespace CleanArchitecture.ShoppingCart.ViewModels
{
    public class BuyableProduct
    {
        public string Description { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public int ProductId { get; set; }
        public int QuantityToBuy { get; set; }
        public Guid ShoppingCartId { get; set; }
        public int RemainingStock { get; set;}
    }
}