﻿using CleanArchitecture.Application.UseCases.ShoppingCarts;
using CleanArchitecture.Domain.Models;
using CleanArchitecture.ShoppingCart.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CleanArchitecture.ShoppingCart.Controllers
{
    [Route("api/[controller]")]

    public class ProductsController : Controller
    {
        private readonly AddProduct addProduct;
        private readonly CheckOut checkOut;
        private readonly ListAllProducts listAllProducts;
        private readonly ListShoppingCart listCart;

        public ProductsController(ListAllProducts listAllProducts, ListShoppingCart listCart, AddProduct addProduct, CheckOut checkOut)
        {
            this.listAllProducts = listAllProducts;
            this.listCart = listCart;
            this.addProduct = addProduct;
            this.checkOut = checkOut;
        }

        [HttpPost("AddToCart")]
        public JsonResult AddToCart([FromBody]BuyableProduct product)
        {
            var cartId = GetOrCreateCartId();

            var result = addProduct.Execute(cartId, product.ProductId, product.QuantityToBuy);

            return Json(result);
        }

        [HttpPost("checkoutCart")]
        public JsonResult CheckoutCart([FromBody] string customerName)
        {
            var cartId = GetOrCreateCartId();

            var result = checkOut.Execute(cartId, customerName);

            ClearShoppingCartSession(result);

            return Json(result);
        }

        [HttpGet("GetAll")]
        public IEnumerable<BuyableProduct> GetAll()
        {
            var cartId = GetOrCreateCartId();

            var prods = listAllProducts.Execute();
            var cartProds = listCart.Execute(cartId);
            var buyable = new List<BuyableProduct>();

            LoadAllProductsInCart(cartId, prods, cartProds, buyable);
            LoadAllAvailableProductsNotInCart(cartId, prods, buyable);

            return buyable;
        }

        private void ClearShoppingCartSession(Common.Functional.Result result)
        {
            if (result.IsSuccess)
                HttpContext.Session.Remove(Constants.ShoppingCartSessionKey);
        }

        private Guid GetOrCreateCartId()
        {
            var cartId = Guid.NewGuid();
            if (!HttpContext.Session.TryGetValue(Constants.ShoppingCartSessionKey, out byte[] idBytes))
                HttpContext.Session.Set(Constants.ShoppingCartSessionKey, cartId.ToByteArray());
            else
                cartId = new Guid(idBytes);
            return cartId;
        }

        private void LoadAllAvailableProductsNotInCart(Guid cartId, IEnumerable<Product> prods, List<BuyableProduct> buyable)
        {
            foreach (var item in prods.Where(x => !buyable.Any(q => q.ProductId == x.Id)))
            {
                buyable.Add(new BuyableProduct
                {
                    ProductId = item.Id,
                    Description = item.Description,
                    Name = item.Name,
                    Price = item.Price,
                    QuantityToBuy = 0,
                    RemainingStock = item.RemainingStock,
                    ShoppingCartId = cartId
                });
            }
        }

        private void LoadAllProductsInCart(Guid cartId, IEnumerable<Product> prods, IEnumerable<CartProduct> cartProds, List<BuyableProduct> buyable)
        {
            foreach (var item in cartProds)
            {
                buyable.Add(new BuyableProduct
                {
                    ProductId = item.ProductId,
                    Description = item.Description,
                    Name = item.Name,
                    Price = item.Price,
                    QuantityToBuy = item.Quantity,
                    RemainingStock = prods.Single(x => x.Id == item.ProductId).RemainingStock,
                    ShoppingCartId = cartId
                });
            }
        }
    }
}