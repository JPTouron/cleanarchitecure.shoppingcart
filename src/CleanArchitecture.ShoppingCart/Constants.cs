﻿namespace CleanArchitecture.ShoppingCart
{
    public class Constants
    {
        public const string ShoppingCartSessionKey = "ShoppingCart";
    }
}