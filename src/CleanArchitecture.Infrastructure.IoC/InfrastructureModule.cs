﻿using CleanArchitecture.Infrastructure.Caching;
using CleanArchitecture.Application.Ports;
using CleanArchitecture.Domain.Models;
using CleanArchitecture.Infrastructure.Repositories;
using CleanArchitecture.Infrastructure.Repositories.EF;
using Ninject.Modules;
using Ninject.Web.Common;
using System;

namespace CleanArchitecture.Infrastructure.IoC
{
    internal class InfrastructureModule : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind<IRepositoryRead<Product, int>>().To<ProductRepository>().InRequestScope();
            Kernel.Bind<IRepositoryPersist<ShoppingCart, Guid>>().To<ShoppingCartRepository>().InRequestScope();

            Kernel.Bind<IInMemoryShoppingCartsManager>().To<InMemoryShoppingCartsManager>().InSingletonScope();
            Kernel.Bind<CleanArchitectureDbContext>().ToSelf().InRequestScope();
        }
    }
}