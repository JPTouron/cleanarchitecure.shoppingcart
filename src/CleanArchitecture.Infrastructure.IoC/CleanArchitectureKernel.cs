﻿using Ninject;

namespace CleanArchitecture.Infrastructure.IoC
{
    public class CleanArchitectureKernel
    {
        private static IKernel kernel;

        public static void Destroy()
        {
            if (kernel != null)
            {
                kernel.Dispose();
                kernel = null;
            }
        }

        public static IKernel GetKernel()
        {
            if (kernel == null)
            {
                kernel = new StandardKernel();
                try
                {
                    LoadModules();

                    return kernel;
                }
                catch
                {
                    kernel.Dispose();
                    throw;
                }
            }
            else
            {
                return kernel;
            }
        }

        private static void LoadModules()
        {
            kernel.Load(new ApplicationModule());
            kernel.Load(new InfrastructureModule());
        }
    }
}