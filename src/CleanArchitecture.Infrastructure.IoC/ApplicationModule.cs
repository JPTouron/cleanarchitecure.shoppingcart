﻿using CleanArchitecture.Application.UseCases.ShoppingCarts;
using Ninject.Modules;
using Ninject.Web.Common;

namespace CleanArchitecture.Infrastructure.IoC
{
    internal class ApplicationModule : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind<ListAllProducts>().ToSelf().InRequestScope();
            Kernel.Bind<AddProduct>().ToSelf().InRequestScope();
            Kernel.Bind<CheckOut>().ToSelf().InRequestScope();
        }
    }
}