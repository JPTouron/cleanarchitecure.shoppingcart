﻿using CleanArchitecture.Application.Ports;
using CleanArchitecture.Application.UseCases.ShoppingCarts;
using CleanArchitecture.Domain.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Linq;

namespace CleanArchitecture.Application.Tests
{
    [TestClass]
    public class ListAllProductsTests
    {
        private Mock<IRepositoryRead<Product, int>> productRepository;
        private ListAllProducts useCase;

        [TestMethod]
        public void Create_WhenUseCaseIsExecuted_ThenAllProductsShouldBeListed()
        {
            var expectedProducts = new List<Product> {
                new Product(2,"TypeScript in Action","Book about TypeScript",32.27m  ,100 ),
                    new Product(3,"Asp.net Core jump start" ,"Book about asp.net core" ,50.2m,100 )
            };

            productRepository.Setup(x => x.GetAll()).Returns(expectedProducts);

            var result = useCase.Execute();

            CollectionAssert.AreEquivalent(expectedProducts, result.ToList());
        }

        [TestInitialize]
        public void Init()
        {
            productRepository = new Mock<IRepositoryRead<Product, int>>();
            useCase = new ListAllProducts(productRepository.Object);
        }
    }
}