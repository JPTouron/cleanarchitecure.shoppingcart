﻿using CleanArchitecture.Application.Messages;
using CleanArchitecture.Application.Ports;
using CleanArchitecture.Application.UseCases.ShoppingCarts;
using CleanArchitecture.Common.Functional;
using CleanArchitecture.Domain.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Linq;

namespace CleanArchitecture.Application.Tests
{
    [TestClass]
    public class AddProductTests
    {
        private Mock<IRepositoryRead<Product, int>> productRepo;
        private Mock<IInMemoryShoppingCartsManager> shoppingCartsCache;
        private AddProduct useCase;

        [TestInitialize]
        public void Init()
        {
            shoppingCartsCache = new Mock<IInMemoryShoppingCartsManager>();
            productRepo = new Mock<IRepositoryRead<Product, int>>();
            useCase = new AddProduct(shoppingCartsCache.Object, productRepo.Object);
        }

        [TestMethod]
        public void WhenExecutedAndNotEnoughStockForProduct_ItReturnsAFailedResultWithErrorMessage()
        {
            var shoppingCartId = Guid.NewGuid();
            var cachedShoppingCart = new ShoppingCart(shoppingCartId);
            var cachedShoppingCartId = Guid.Empty;
            var productId = 3;
            var prodInStock = 4;
            var expectedProduct = new Product(productId, "Asp.net Core jump start", "Book about asp.net core", 50.2m, prodInStock);
            var expectedProductQuantity = 5;

            var expectedErrorMessage = ErrorMessages.NoAvailableStock;

            productRepo.Setup(x => x.GetById(productId)).Returns(expectedProduct);

            var result = useCase.Execute(shoppingCartId, productId, expectedProductQuantity);

            Assert.IsTrue(result.IsFailure);
            Assert.AreEqual(expectedErrorMessage, result.ErrorMessage);
        }

        [TestMethod]
        public void WhenExecutedForFirstTime_ItCreatesAShoppingCartSavesItIntoTheCacheAndAddsTheProducstToIt_ItReturnsOk()
        {
            var shoppingCartId = Guid.NewGuid();
            var cachedShoppingCart = new ShoppingCart(shoppingCartId);
            var cachedShoppingCartId = Guid.Empty;
            var productId = 3;
            var expectedProduct = new Product(productId, "Asp.net Core jump start", "Book about asp.net core", 50.2m, 100);
            var expectedProductQuantity = 5;

            productRepo.Setup(x => x.GetById(productId)).Returns(expectedProduct);
            shoppingCartsCache.Setup(x => x.GetOrAdd(It.IsAny<ShoppingCart>()))
                .Callback((ShoppingCart sc) =>
                {
                    cachedShoppingCartId = sc.Id;
                })
                .Returns(cachedShoppingCart);

            var result = useCase.Execute(shoppingCartId, productId, expectedProductQuantity);

            var cartProduct = cachedShoppingCart.GetProductsInCart().First();

            Assert.AreEqual(result, Result.Success());

            Assert.AreEqual(expectedProduct.Id, cartProduct.ProductId);
            Assert.AreEqual(expectedProduct.Name, cartProduct.Name);
            Assert.AreEqual(expectedProduct.Description, cartProduct.Description);
            Assert.AreEqual(expectedProduct.Price, cartProduct.Price);
            Assert.AreEqual(expectedProductQuantity, cartProduct.Quantity);
            Assert.AreEqual(shoppingCartId, cartProduct.ShoppingCartId);

            Assert.AreEqual(cachedShoppingCartId, shoppingCartId);
        }

        [TestMethod]
        public void WhenExecutedForSecondTime_ThenItGetsAShoppingCartExistingInTheCacheAndUpdatesItsContents_ItReturnsOk()
        {
            var shoppingCartId = Guid.NewGuid();
            var shoppingCart = new ShoppingCart(shoppingCartId);
            var productId = 3;
            var expectedProduct = new Product(productId, "Asp.net Core jump start", "Book about asp.net core", 50.2m, 100);

            var productQuantityFirst = 1;
            var productQuantitySecond = 3;

            var totalExpectedQuantity = productQuantityFirst + productQuantitySecond;

            shoppingCartsCache.Setup(x => x.GetOrAdd(It.IsAny<ShoppingCart>())).Returns(shoppingCart);
            productRepo.Setup(x => x.GetById(productId)).Returns(expectedProduct);

            var resultFirst = useCase.Execute(shoppingCartId, productId, productQuantityFirst);
            var resultSecond = useCase.Execute(shoppingCartId, productId, productQuantitySecond);

            Assert.AreEqual(resultFirst, Result.Success(), resultFirst.ErrorMessage);
            Assert.AreEqual(resultSecond, Result.Success(), resultSecond.ErrorMessage);

            var cartProduct = shoppingCart.GetProductsInCart().Single();

            Assert.AreEqual(expectedProduct.Id, cartProduct.ProductId);
            Assert.AreEqual(expectedProduct.Name, cartProduct.Name);
            Assert.AreEqual(expectedProduct.Description, cartProduct.Description);
            Assert.AreEqual(expectedProduct.Price, cartProduct.Price);
            Assert.AreEqual(shoppingCartId, cartProduct.ShoppingCartId);
            Assert.AreEqual(productQuantitySecond, cartProduct.Quantity);
        }

        [TestMethod]
        public void WhenExecutedForTwoCarts_ThenItCreatesTwoShoppingCarts()
        {
            var shoppingCartId1 = Guid.NewGuid();
            var shoppingCartId2 = Guid.NewGuid();
            var shoppingCart1 = new ShoppingCart(shoppingCartId1);
            var shoppingCart2 = new ShoppingCart(shoppingCartId2);
            var expectedProductCountInCart1 = 1;
            var expectedProductCountInCart2 = 1;

            var requestedCartIdFromCache = Guid.Empty;

            var prodId1 = 3;
            var prodId2 = 2;
            var product1 = new Product(3, "Asp.net Core jump start", "Book about asp.net core", 50.2m, 100);
            var product2 = new Product(2, "TypeScript in Action", "Book about TypeScript", 32.27m, 100);

            var productQuantityFirst = 1;
            var productQuantitySecond = 3;

            shoppingCartsCache.Setup(x => x.GetOrAdd(It.IsAny<ShoppingCart>()))
                .Callback((ShoppingCart sc) => { requestedCartIdFromCache = sc.Id; })
                .Returns(() =>
                {
                    if (requestedCartIdFromCache == shoppingCart1.Id)
                        return shoppingCart1;
                    else
                        return shoppingCart2;
                });

            productRepo.Setup(x => x.GetById(prodId1)).Returns(product1);
            productRepo.Setup(x => x.GetById(prodId2)).Returns(product2);

            useCase.Execute(shoppingCartId1, prodId1, productQuantityFirst);
            useCase.Execute(shoppingCartId2, prodId2, productQuantitySecond);

            Assert.AreEqual(expectedProductCountInCart1, shoppingCart1.GetProductsInCart().Count);
            Assert.AreEqual(expectedProductCountInCart2, shoppingCart2.GetProductsInCart().Count);
            Assert.AreEqual(prodId1, shoppingCart1.GetProductsInCart().Single().ProductId);
            Assert.AreEqual(prodId2, shoppingCart2.GetProductsInCart().Single().ProductId);
        }
    }
}