﻿using CleanArchitecture.Application.Messages;
using CleanArchitecture.Application.Ports;
using CleanArchitecture.Application.UseCases.ShoppingCarts;
using CleanArchitecture.Common.Functional;
using CleanArchitecture.Domain.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CleanArchitecture.Application.Tests
{
    [TestClass]
    public class CheckoutTests
    {
        private Mock<IRepositoryPersist<ShoppingCart, Guid>> purchaseRepo;
        private Mock<IInMemoryShoppingCartsManager> shoppingCartManager;
        private CheckOut useCase;

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Execute_WhenCustomerNameIsEmpty_ThenThrowsException()
        {
            useCase.Execute(Guid.NewGuid(), string.Empty);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Execute_WhenCustomerNameIsWhiteSpace_ThenThrowsException()
        {
            useCase.Execute(Guid.NewGuid(), " ");
        }

        [TestMethod]
        public void Execute_WhenShoppingCartHasProducts_ThenReturnsOk()
        {
            var expectedShoppingCartId = Guid.NewGuid();
            var theShoppingCart = new ShoppingCart(expectedShoppingCartId);
            var aProduct = new Product(3, "Asp.net Core jump start", "Book about asp.net core", 50.2m, 100);
            var cartProduct = new CartProduct(Guid.NewGuid(), theShoppingCart.Id, aProduct, 5);
            var expectedResult = Result.Success();
            var exepctedCustomerName = "John Doe";
            var expectedPurchaseDate = DateTime.Today;
            IEnumerable<CartProduct> expectedProductsInCart = null;

            theShoppingCart.AddOrUpdateProductInCart(cartProduct);

            shoppingCartManager.Setup(x => x.GetOrAdd(It.IsAny<ShoppingCart>())).Returns(theShoppingCart);
            purchaseRepo.Setup(x => x.Insert(theShoppingCart))
                .Callback((ShoppingCart sc) =>
                {
                    expectedProductsInCart = sc.GetProductsInCart();
                })
                .Returns(expectedResult).Verifiable();

            var result = useCase.Execute(expectedShoppingCartId, exepctedCustomerName);

            Assert.AreEqual(expectedResult, result);
            Assert.IsTrue(theShoppingCart.IsPurchased);
            Assert.AreEqual(exepctedCustomerName, theShoppingCart.PurchaseOrder.CustomerName);
            Assert.AreEqual(expectedPurchaseDate.ToShortDateString(), theShoppingCart.PurchaseOrder.PurchaseDate.ToShortDateString());
            CollectionAssert.AreEquivalent(expectedProductsInCart.ToList(), theShoppingCart.GetProductsInCart().ToList());

            purchaseRepo.Verify(x => x.Insert(theShoppingCart), Times.Once);
        }

        [TestMethod]
        public void Execute_WhenShoppingCartHasProductsAndRepositoryFails_ThenReturnsFail()
        {
            var expectedShoppingCartId = Guid.NewGuid();
            var theShoppingCart = new ShoppingCart(expectedShoppingCartId);
            var aProduct = new Product(3, "Asp.net Core jump start", "Book about asp.net core", 50.2m, 100);
            var cartProduct = new CartProduct(Guid.NewGuid(), theShoppingCart.Id, aProduct, 5);
            var expectedResult = Result.Success();
            var expectedRepoResult = Result.Fail("Could not save theshopping cart");
            var exepctedCustomerName = "John Doe";
            var expectedPurchaseDate = DateTime.Today;

            theShoppingCart.AddOrUpdateProductInCart(cartProduct);

            shoppingCartManager.Setup(x => x.GetOrAdd(It.IsAny<ShoppingCart>())).Returns(theShoppingCart);
            purchaseRepo.Setup(x => x.Insert(theShoppingCart)).Returns(expectedRepoResult).Verifiable();

            var result = useCase.Execute(expectedShoppingCartId, exepctedCustomerName);

            Assert.AreEqual(expectedRepoResult, result);
            Assert.IsTrue(theShoppingCart.IsPurchased);
            Assert.AreEqual(exepctedCustomerName, theShoppingCart.PurchaseOrder.CustomerName);
            Assert.AreEqual(expectedPurchaseDate.ToShortDateString(), theShoppingCart.PurchaseOrder.PurchaseDate.ToShortDateString());

            purchaseRepo.Verify(x => x.Insert(theShoppingCart), Times.Once);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Execute_WhenShoppingCartIdIsInvalid_ThenThrowsException()
        {
            useCase.Execute(Guid.Empty, "john doe");
        }

        [TestMethod]
        public void Execute_WhenShoppingCartIsEmpty_ThenReturnsError()
        {
            shoppingCartManager.Setup(x => x.GetOrAdd(It.IsAny<ShoppingCart>())).Returns(new ShoppingCart(Guid.NewGuid()));
            var expectedResult = Result.Fail(ErrorMessages.NoProductsInCart);

            var result = useCase.Execute(Guid.NewGuid(), "John Doe");

            Assert.AreEqual(expectedResult, result);
        }

        [TestInitialize]
        public void Init()
        {
            purchaseRepo = new Mock<IRepositoryPersist<ShoppingCart, Guid>>();
            shoppingCartManager = new Mock<IInMemoryShoppingCartsManager>();
            useCase = new CheckOut(purchaseRepo.Object, shoppingCartManager.Object);
        }
    }
}