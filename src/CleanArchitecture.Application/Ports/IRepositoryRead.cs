﻿using CleanArchitecture.Common.Functional;
using CleanArchitecture.Domain;
using System.Collections.Generic;

namespace CleanArchitecture.Application.Ports
{
    public interface IRepositoryRead<T, TId> where T : BaseEntity<TId>
    {
        /// <summary>
        /// For simplicity we do not handle pagination, but we could easily do it by implementing 2 parameters in this method:
        /// int page, int itemsPerPage
        /// </summary>
        IEnumerable<T> GetAll();

        Maybe<T> GetById(TId id);
    }
}