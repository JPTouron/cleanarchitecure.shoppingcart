﻿using CleanArchitecture.Domain.Models;
using System;

namespace CleanArchitecture.Application.Ports
{
    public interface IInMemoryShoppingCartsManager
    {
        int Count();

        ShoppingCart GetOrAdd(ShoppingCart cart);

        void RemoveCart(Guid cartId);
    }
}