﻿using CleanArchitecture.Common.Functional;
using CleanArchitecture.Domain;

namespace CleanArchitecture.Application.Ports
{
    public interface IRepositoryPersist<T, TId> where T : BaseEntity<TId>
    {
        Result Insert(T entity);
    }
}