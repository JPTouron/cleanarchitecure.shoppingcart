﻿using CleanArchitecture.Common.Guards;
using CleanArchitecture.Domain.Models;
using System;

namespace CleanArchitecture.Application
{
    public static class ProductMappings
    {
        public static CartProduct ToCartProduct(this Product product, int quantity, Guid shoppingCartId)
        {
            Guard.Against.Null(product, nameof(product));
            Guard.Against.EmptyOrNullGuid(shoppingCartId, nameof(shoppingCartId));

            var cp = new CartProduct(Guid.NewGuid(), shoppingCartId, product, quantity);

            return cp;
        }
    }
}