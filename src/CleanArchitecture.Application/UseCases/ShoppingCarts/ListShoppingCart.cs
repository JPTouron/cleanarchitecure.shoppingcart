﻿using CleanArchitecture.Application.Ports;
using CleanArchitecture.Domain.Models;
using System;
using System.Collections.Generic;

namespace CleanArchitecture.Application.UseCases.ShoppingCarts
{
    public class ListShoppingCart
    {
        private IInMemoryShoppingCartsManager shoppingCartManager;

        public ListShoppingCart(IInMemoryShoppingCartsManager shoppingCartManager)
        {
            this.shoppingCartManager = shoppingCartManager;
        }

        public IEnumerable<CartProduct> Execute(Guid shoppingCartId)
        {
            var sc = GetShoppingCart(shoppingCartId);
            var products = GetProducts(sc);

            return products;
        }

        private static IEnumerable<CartProduct> GetProducts(ShoppingCart sc)
        {
            return sc.GetProductsInCart();
        }

        private ShoppingCart GetShoppingCart(Guid shoppingCartId)
        {
            var sc = new ShoppingCart(shoppingCartId);
            sc = shoppingCartManager.GetOrAdd(sc);
            return sc;
        }
    }
}