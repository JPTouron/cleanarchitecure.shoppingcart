﻿using CleanArchitecture.Application.Messages;
using CleanArchitecture.Application.Ports;
using CleanArchitecture.Common.Functional;
using CleanArchitecture.Common.Guards;
using CleanArchitecture.Domain.Models;
using System;
using System.Linq;

namespace CleanArchitecture.Application.UseCases.ShoppingCarts
{
    public class CheckOut
    {
        private IInMemoryShoppingCartsManager shoppingCartManager;
        private IRepositoryPersist<ShoppingCart, Guid> shoppingRepo;

        public CheckOut(IRepositoryPersist<ShoppingCart, Guid> shoppingRepo, IInMemoryShoppingCartsManager shoppingCartManager)
        {
            this.shoppingRepo = shoppingRepo;
            this.shoppingCartManager = shoppingCartManager;
        }

        public Result Execute(Guid shoppingCartId, string customerName)
        {
            ValidateInvariants(shoppingCartId, customerName);

            var sc = GetShoppingCart(shoppingCartId);

            var result = ValidateShoppingCart(sc);

            if (result.IsSuccess)
                result = PurchaseAndTryToPersistShoppingCart(customerName, sc);

            return result;
        }

        private ShoppingCart GetShoppingCart(Guid shoppingCartId)
        {
            var sc = new ShoppingCart(shoppingCartId);
            sc = shoppingCartManager.GetOrAdd(sc);

            return sc;
        }

        private Result PurchaseAndTryToPersistShoppingCart(string customerName, ShoppingCart sc)
        {
            sc.PurchaseProductsInCart(customerName);
            return shoppingRepo.Insert(sc);
        }

        private void ValidateInvariants(Guid shoppingCartId, string customerName)
        {
            Guard.Against.EmptyOrNullGuid(shoppingCartId, nameof(shoppingCartId));
            Guard.Against.NullOrWhiteSpace(customerName, nameof(customerName));
        }

        private Result ValidateShoppingCart(ShoppingCart sc)
        {
            if (sc.GetProductsInCart().Count() > 0)
                return Result.Success();
            else
                return Result.Fail(ErrorMessages.NoProductsInCart);
        }
    }
}