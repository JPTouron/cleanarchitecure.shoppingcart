﻿using CleanArchitecture.Application.Ports;
using CleanArchitecture.Common.Guards;
using CleanArchitecture.Domain.Models;
using System.Collections.Generic;

namespace CleanArchitecture.Application.UseCases.ShoppingCarts
{
    /// <summary>
    /// Use case for Excersice requirement: User Story 1: "As a customer, I want to be able to see a list of available products, so that I can determine what I can buy."
    /// </summary>
    public class ListAllProducts
    {
        private IRepositoryRead<Product, int> productRepository;

        public ListAllProducts(IRepositoryRead<Product, int> repository)
        {
            Guard.Against.Null(repository, nameof(repository));
            productRepository = repository;
        }

        public IEnumerable<Product> Execute()
        {
            return productRepository.GetAll();
        }
    }
}