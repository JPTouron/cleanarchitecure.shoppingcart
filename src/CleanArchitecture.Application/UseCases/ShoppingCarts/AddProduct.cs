﻿using CleanArchitecture.Application.Messages;
using CleanArchitecture.Application.Ports;
using CleanArchitecture.Common.Functional;
using CleanArchitecture.Common.Guards;
using CleanArchitecture.Domain.Models;
using System;

namespace CleanArchitecture.Application.UseCases.ShoppingCarts
{
    public class AddProduct
    {
        private IRepositoryRead<Product, int> productRepository;
        private IInMemoryShoppingCartsManager shoppingCartManager;

        public AddProduct(IInMemoryShoppingCartsManager shoppingCartManager, IRepositoryRead<Product, int> productRepository)
        {
            Guard.Against.Null(shoppingCartManager, nameof(shoppingCartManager));
            Guard.Against.Null(productRepository, nameof(productRepository));

            this.productRepository = productRepository;
            this.shoppingCartManager = shoppingCartManager;
        }

        public Result Execute(Guid shoppingCartId, int productId, int quantity)
        {
            ValidateInvariants(shoppingCartId, productId);

            if (quantity < 1 || quantity > int.MaxValue)
                return Result.Fail(ErrorMessages.CannotAddProductWithQuantityZero);

            var result = Result.Success();

            result = TryToGetProductAndTryToAddItToCart(shoppingCartId, productId, quantity);

            return result;
        }

        private static void ValidateInvariants(Guid shoppingCartId, int productId)
        {
            Guard.Against.EmptyOrNullGuid(shoppingCartId, nameof(shoppingCartId));
            Guard.Against.OutOfRange(productId, nameof(productId), 1, int.MaxValue);
        }

        private void AddProductToCart(Guid ShoppingCartId, Product product, int quantity)
        {
            var sc = new ShoppingCart(ShoppingCartId);
            var cart = shoppingCartManager.GetOrAdd(sc);

            cart.AddOrUpdateProductInCart(product.ToCartProduct(quantity, cart.Id));
        }

        private Maybe<Product> LookupProduct(int productId)
        {
            return productRepository.GetById(productId);
        }

        private Result TryToAddProductToCart(Guid ShoppingCartId, int quantity, Product product)
        {
            var result = Result.Success();

            if (quantity > product.RemainingStock)
                result = Result.Fail(ErrorMessages.NoAvailableStock);
            else
                AddProductToCart(ShoppingCartId, product, quantity);

            return result;
        }

        private Result<Product> TryToGetProduct(int productId)
        {
            var result = Result.FailWithDefaultReturnValue<Product>(ErrorMessages.NoProductFound);
            var maybeProduct = LookupProduct(productId);

            if (maybeProduct.HasValue)
                result = Result.SuccessWithReturnValue(maybeProduct.Value);

            return result;
        }

        private Result TryToGetProductAndTryToAddItToCart(Guid ShoppingCartId, int productId, int quantity)
        {
            var mainResult = Result.Fail(ErrorMessages.NoProductFound);

            var getProductResult = TryToGetProduct(productId);

            if (getProductResult.IsSuccess)
                mainResult = TryToAddProductToCart(ShoppingCartId, quantity, getProductResult.Value);

            return mainResult;
        }
    }
}