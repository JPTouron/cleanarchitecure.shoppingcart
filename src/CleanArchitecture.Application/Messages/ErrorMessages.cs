﻿namespace CleanArchitecture.Application.Messages
{
    public static class ErrorMessages
    {
        public const string CannotAddProductWithQuantityZero = "Cannot add product to cart with quantity zero";
        public const string CouldNotSaveShoppingCart = "Could not save theshopping cart.";
        public const string NoAvailableStock = "The quantity you have selected for the product exceeds the available stock.";
        public const string NoProductFound = "The product you selected could not be found.";
        public const string NoProductsInCart = "There are no products in your shopping cart. Cannot check out the Shopping cart.";
    }
}