﻿using CleanArchitecture.Infrastructure.Repositories.DataModel;
using Microsoft.EntityFrameworkCore;

namespace CleanArchitecture.Infrastructure.Repositories.EF
{
    public class CleanArchitectureDbContext : DbContext
    {
        public CleanArchitectureDbContext(DbContextOptions options) : base(options)
        {
        }

        public CleanArchitectureDbContext()
        {
        }

        //entities
        public DbSet<CartProduct> CartProducts { get; private set; }

        public DbSet<Product> Products { get; private set; }

        public DbSet<ShoppingCart> ShoppingCarts { get; private set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                var connection = @"Server=(localdb)\mssqllocaldb;Database=CleanArchitectureDb;Trusted_Connection=True;ConnectRetryCount=0";

                optionsBuilder.UseSqlServer(connection);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Product>().HasData(
                new Product { Id = 1, Name = "Angular Ready", Description = "book about Angular", Price = 20.2m, RemainingStock = 100 },
                new Product { Id = 2, Name = "TypeScript in Action", Description = "Book about TypeScript", Price = 32.27m, RemainingStock = 100 },
                new Product { Id = 3, Name = "Asp.net Core jump start", Description = "Book about asp.net core", Price = 50.2m, RemainingStock = 100 },
                new Product { Id = 4, Name = "Docker for dummies ", Description = "Book about docker", Price = 15.6m, RemainingStock = 2 }
                );
        }
    }
}