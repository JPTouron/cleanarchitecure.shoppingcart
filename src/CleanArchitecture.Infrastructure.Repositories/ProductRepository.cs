﻿using CleanArchitecture.Application.Ports;
using CleanArchitecture.Common.Functional;
using CleanArchitecture.Domain.Models;
using CleanArchitecture.Infrastructure.Repositories.EF;
using CleanArchitecture.Infrastructure.Repositories.Mappings;
using System.Collections.Generic;
using System.Linq;

namespace CleanArchitecture.Infrastructure.Repositories
{
    public class ProductRepository : IRepositoryRead<Product, int>
    {
        private readonly CleanArchitectureDbContext db;

        public ProductRepository(CleanArchitectureDbContext db)
        {
            this.db = db;
        }

        public IEnumerable<Product> GetAll()
        {
            IEnumerable<DataModel.Product> products;

            using (db)
            {
                products = db.Products.ToList();
            }

            return products.ToProducts();
        }

        public Maybe<Product> GetById(int id)
        {
            using (db)

            {
                var prod = db.Products.SingleOrDefault(x => x.Id == id);

                var result = Maybe<Product>.From(prod.ToProduct());

                return result;
            }
        }
    }
}