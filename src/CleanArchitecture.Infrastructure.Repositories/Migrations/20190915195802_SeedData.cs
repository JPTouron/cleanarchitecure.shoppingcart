﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CleanArchitecture.Infrastructure.Repositories.Migrations
{
    public partial class SeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Description", "Name", "Price", "RemainingStock" },
                values: new object[,]
                {
                    { 1, "book about Angular", "Angular Ready", 20.2m, 100 },
                    { 2, "Book about TypeScript", "TypeScript in Action", 32.27m, 100 },
                    { 3, "Book about asp.net core", "Asp.net Core jump start", 50.2m, 100 },
                    { 4, "Book about docker", "Docker for dummies ", 15.6m, 2 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 4);
        }
    }
}
