﻿namespace CleanArchitecture.Infrastructure.Repositories.DataModel
{
    public abstract class BaseEntity<TId>
    {
        public TId Id { get; set; }
    }
}