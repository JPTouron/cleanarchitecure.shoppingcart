﻿using System;

namespace CleanArchitecture.Infrastructure.Repositories.DataModel
{
    public class CartProduct : BaseEntity<Guid>
    {
        public CartProduct()
        {
        }

        public string Description { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public int ProductId { get; set; }
        public int Quantity { get; set; }
        public Guid ShoppingCartId { get; set; }
    }
}