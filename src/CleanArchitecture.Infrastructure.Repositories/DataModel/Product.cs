﻿namespace CleanArchitecture.Infrastructure.Repositories.DataModel
{
    public class Product : BaseEntity<int>
    {
        public Product()
        {
        }

        public string Description { get; set; }

        public string Name { get; set; }

        public decimal Price { get; set; }

        public int RemainingStock { get; set; }
    }
}