﻿using System;
using System.Collections.Generic;

namespace CleanArchitecture.Infrastructure.Repositories.DataModel
{
    public class ShoppingCart : BaseEntity<Guid>
    {
        public ShoppingCart()
        {
            products = new List<CartProduct>();
        }

        public string CustomerName { get; set; }
        public IList<CartProduct> products { get; set; }
        public DateTime PurchaseDate { get; set; }
    }
}