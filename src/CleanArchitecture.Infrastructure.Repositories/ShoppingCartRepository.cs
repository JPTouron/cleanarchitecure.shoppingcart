﻿using CleanArchitecture.Application.Ports;
using CleanArchitecture.Common.Functional;
using CleanArchitecture.Domain.Models;
using CleanArchitecture.Infrastructure.Repositories.EF;
using CleanArchitecture.Infrastructure.Repositories.Mappings;
using System;
using System.Linq;

namespace CleanArchitecture.Infrastructure.Repositories
{
    public class ShoppingCartRepository : IRepositoryPersist<ShoppingCart, Guid>
    {
        private readonly CleanArchitectureDbContext db;

        public ShoppingCartRepository(CleanArchitectureDbContext db)
        {
            this.db = db;
        }

        public Result Insert(ShoppingCart entity)
        {
            var result = Result.Success();

            var cartProds = entity.GetProductsInCart().ToCartProduct();
            var sc = entity.ToShoppingCart();
            using (db)

            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        db.ShoppingCarts.Add(sc);
                        db.SaveChanges();

                        foreach (var cartProduct in cartProds)
                        {
                            DataModel.Product product = GetTheProductFromDatabase(db, cartProduct);

                            if (IsThereEnoughStock(cartProduct, product))
                                AddCartProductAndUpdateProductQuantity(db, cartProduct, product);
                            else
                                result = Result.Fail($"There's not enough stock of {cartProduct.Name} for you to buy. We only have {product.RemainingStock} remaining.");
                        }

                        transaction.Commit();
                    }
                    catch //this could be improved with specific-exception catching
                    {
                        transaction.Rollback();

                        //log the transaction...

                        result = Result.Fail("We could not save your purchase, please try again!");
                    }
                }
            }

            return result;
        }

        private static void AddCartProductAndUpdateProductQuantity(CleanArchitectureDbContext db, DataModel.CartProduct cartProduct, DataModel.Product product)
        {
            db.CartProducts.Add(cartProduct);

            product.RemainingStock = product.RemainingStock - cartProduct.Quantity;

            db.Products.Update(product);
            db.SaveChanges();
        }

        private static DataModel.Product GetTheProductFromDatabase(CleanArchitectureDbContext db, DataModel.CartProduct cartProduct)
        {
            return db.Products.Single(x => x.Id == cartProduct.ProductId);
        }

        private static bool IsThereEnoughStock(DataModel.CartProduct cartProduct, DataModel.Product product)
        {
            return product.RemainingStock >= cartProduct.Quantity;
        }
    }
}