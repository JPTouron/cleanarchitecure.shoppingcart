﻿using CleanArchitecture.Domain.Models;
using System.Collections.Generic;

namespace CleanArchitecture.Infrastructure.Repositories.Mappings
{
    public static class Mappings
    {
        public static IEnumerable<DataModel.CartProduct> ToCartProduct(this IEnumerable<CartProduct> p)
        {
            foreach (var item in p)
            {
                yield return item.ToCartProduct();
            }
        }

        public static DataModel.CartProduct ToCartProduct(this CartProduct p)
        {
            var cp = new DataModel.CartProduct
            {
                Description = p.Description,
                Id = p.Id,
                Name = p.Name,
                Price = p.Price,
                ProductId = p.ProductId,
                Quantity = p.Quantity,
                ShoppingCartId = p.ShoppingCartId
            };

            return cp;
        }

        public static Product ToProduct(this DataModel.Product p)
        {
            if (p == null)
                return null;

            var pr = new Product(p.Id, p.Name, p.Description, p.Price, p.RemainingStock);

            return pr;
        }

        public static IEnumerable<Product> ToProducts(this IEnumerable<DataModel.Product> p)
        {
            foreach (var item in p)
            {
                yield return item.ToProduct();
            }
        }

        public static DataModel.ShoppingCart ToShoppingCart(this ShoppingCart p)
        {
            var cp = new DataModel.ShoppingCart
            {
                CustomerName = p.PurchaseOrder.CustomerName,
                Id = p.Id,
                PurchaseDate = p.PurchaseOrder.PurchaseDate
            };

            return cp;
        }
    }
}