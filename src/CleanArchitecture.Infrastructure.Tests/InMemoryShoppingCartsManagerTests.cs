﻿using CleanArchitecture.Infrastructure.Caching;
using CleanArchitecture.Domain.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace CleanArchitecture.Infrastructure.Tests
{
    [TestClass]
    public class InMemoryShoppingCartsManagerTests
    {
        private InMemoryShoppingCartsManager shoppingCartsManager;

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetOrAdd_ThrowsExceptionWhenShoppingCartIsNull()
        {
            shoppingCartsManager.GetOrAdd(null);
        }

        [TestMethod]
        public void GetOrAdd_WhenCartIsInTheCacheAndAnotherIsAdded_ThenCountIncreases()
        {
            var expectedGuidIdA = Guid.NewGuid();
            var expectedGuidIdB = Guid.NewGuid();
            var cartA = new ShoppingCart(expectedGuidIdA);
            var cartB = new ShoppingCart(expectedGuidIdB);
            var expectedCarCount = 2;

            shoppingCartsManager.GetOrAdd(cartA);//add the cart
            shoppingCartsManager.GetOrAdd(cartB);//add the cart

            var retrievedCartA = shoppingCartsManager.GetOrAdd(cartA);
            var retrievedCartB = shoppingCartsManager.GetOrAdd(cartB);

            Assert.AreEqual(expectedCarCount, shoppingCartsManager.Count());
            Assert.AreEqual(retrievedCartA.Id, cartA.Id);
            Assert.AreSame(retrievedCartA, cartA);
            Assert.AreEqual(retrievedCartB.Id, cartB.Id);
            Assert.AreSame(retrievedCartB, cartB);
        }

        [TestMethod]
        public void GetOrAdd_WhenCartIsInTheCacheItGetsRetrieved()
        {
            var expectedGuidId = Guid.NewGuid();
            var cart = new ShoppingCart(expectedGuidId);

            shoppingCartsManager.GetOrAdd(cart);//add the cart
            var result = shoppingCartsManager.GetOrAdd(new ShoppingCart(expectedGuidId)); //retrieve the same cart

            Assert.AreEqual(expectedGuidId, result.Id);
        }

        [TestMethod]
        public void GetOrAdd_WhenShoppingCartIsNotFoundThenItGetsAddedIntoTheCache()
        {
            var expectedGuid = Guid.NewGuid();
            var expectedCarCount = 1;

            var result = shoppingCartsManager.GetOrAdd(new ShoppingCart(expectedGuid));

            Assert.AreEqual(expectedGuid, result.Id);
            Assert.AreEqual(expectedCarCount, shoppingCartsManager.Count());
        }

        [TestInitialize]
        public void Init()
        {
            shoppingCartsManager = new InMemoryShoppingCartsManager();
        }
    }
}