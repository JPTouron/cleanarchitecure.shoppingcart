﻿using CleanArchitecture.Infrastructure.Repositories;
using CleanArchitecture.Infrastructure.Repositories.DataModel;
using CleanArchitecture.Infrastructure.Repositories.EF;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace CleanArchitecture.Infrastructure.Tests
{
    [TestClass]
    public class ProductRepositoryTests
    {
        private DbContextOptions<CleanArchitectureDbContext> dbOptions;
        private ProductRepository repo;

        [TestMethod]
        public void GetAll_WhenExecuted_GetsAllFourItems()
        {
            var expectedCount = 4;

            var result = repo.GetAll();

            Assert.AreEqual(expectedCount, result.Count());
        }

        [TestInitialize]
        public void Init()
        {
            dbOptions = new DbContextOptionsBuilder<CleanArchitectureDbContext>().UseInMemoryDatabase(databaseName: "CleanArchitectureDbContext").Options;

            using (var db = new CleanArchitectureDbContext(dbOptions))
            {
                db.Products.Add(new Product { Id = 1, Name = "Angular Ready", Description = "book about Angular", Price = 20.2m, RemainingStock = 100 });
                db.Products.Add(new Product { Id = 2, Name = "TypeScript in Action", Description = "Book about TypeScript", Price = 32.27m, RemainingStock = 100 });
                db.Products.Add(new Product { Id = 3, Name = "Asp.net Core jump start", Description = "Book about asp.net core", Price = 50.2m, RemainingStock = 100 });
                db.Products.Add(new Product { Id = 4, Name = "Docker for dummies ", Description = "Book about docker", Price = 15.6m, RemainingStock = 2 });
                db.SaveChanges();
            }
            var db2 = new CleanArchitectureDbContext(dbOptions);
            repo = new ProductRepository(db2);
        }
    }
}