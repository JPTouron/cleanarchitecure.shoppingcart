﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CleanArchitecture.Domain
{
    /// <summary>
    /// Represents a Base class for all ValueObjects in the application model
    /// </summary>
    public abstract class BaseValueObject<T> where T : BaseValueObject<T>
    {
        public static bool operator !=(BaseValueObject<T> a, BaseValueObject<T> b)
        {
            return !(a == b);
        }

        public static bool operator ==(BaseValueObject<T> a, BaseValueObject<T> b)
        {
            if (ReferenceEquals(a, null) && ReferenceEquals(b, null))
                return true;

            if (ReferenceEquals(a, null) || ReferenceEquals(b, null))
                return false;

            return a.Equals(b);
        }

        public override bool Equals(object obj)
        {
            var valueObject = obj as BaseValueObject<T>;

            if (ReferenceEquals(valueObject, null))
                return false;

            if (GetType() != obj.GetType())
                return false;

            return GetEqualityComponents().SequenceEqual(valueObject.GetEqualityComponents());
        }

        public override int GetHashCode()
        {
            return GetEqualityComponents()
                .Aggregate(1, (current, obj) =>
                {
                    unchecked
                    {
                        return current * 23 + (obj?.GetHashCode() ?? 0);
                    }
                });
        }

        /// <summary>
        /// returns a list of the object's properties that would be used for equality comparison logic
        /// </summary>
        protected abstract IEnumerable<object> GetEqualityComponents();
    }
}