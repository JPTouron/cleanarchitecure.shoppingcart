﻿namespace CleanArchitecture.Domain
{
    /// <summary>
    /// represents a base entity for a ddd model
    /// </summary>
    public abstract class BaseEntity<TId>
    {
        protected BaseEntity(TId id)
        {
            Id = id;
        }

        public TId Id { get; }
    }
}