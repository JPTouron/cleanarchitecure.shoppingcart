﻿using CleanArchitecture.Application.Ports;
using CleanArchitecture.Common.Functional;
using CleanArchitecture.Common.Guards;
using CleanArchitecture.Domain.Models;
using System;
using System.Collections.Generic;

namespace CleanArchitecture.Infrastructure.Caching
{
    public sealed class InMemoryShoppingCartsManager : IInMemoryShoppingCartsManager
    {
        private IDictionary<Guid, ShoppingCart> shoppingCartsCache;

        public InMemoryShoppingCartsManager()
        {
            shoppingCartsCache = new Dictionary<Guid, ShoppingCart>();
        }

        public int Count()
        {
            return shoppingCartsCache.Count;
        }

        public ShoppingCart GetOrAdd(ShoppingCart cart)
        {
            Guard.Against.Null(cart, nameof(cart));

            var cachedCart = GetCart(cart.Id);

            if (cachedCart.HasNoValue)
                AddShoppingCart(cart);
            else
                cart = cachedCart.Value;

            return cart;
        }

        public void RemoveCart(Guid cartId)
        {
            Guard.Against.EmptyOrNullGuid(cartId, nameof(cartId));

            shoppingCartsCache.Remove(cartId);
        }

        private void AddShoppingCart(ShoppingCart cart)
        {
            shoppingCartsCache.Add(cart.Id, cart);
        }

        private Maybe<ShoppingCart> GetCart(Guid cartId)
        {
            shoppingCartsCache.TryGetValue(cartId, out ShoppingCart cart);

            var result = Maybe<ShoppingCart>.From(cart);

            return result;
        }
    }
}