﻿using CleanArchitecture.Common.Guards;

namespace CleanArchitecture.Domain.Models
{
    public class Product : BaseEntity<int>
    {
        public Product(int id, string name, string description, decimal price, int remainingStock) : base(id)
        {
            ValidateInvariants(id, name, description, price, remainingStock);

            Description = description;
            Name = name;
            Price = price;
            RemainingStock = remainingStock;
        }

        public string Description { get; }

        public string Name { get; }

        public decimal Price { get; }

        public int RemainingStock { get; }

        private void ValidateInvariants(int id, string name, string description, decimal price, int remainingStock)
        {
            Guard.Against.OutOfRange(id, nameof(id), 1, int.MaxValue);
            Guard.Against.NullOrWhiteSpace(name, nameof(name));
            Guard.Against.NullOrWhiteSpace(description, nameof(description));
            Guard.Against.OutOfRange(price, nameof(price), 0.01m, decimal.MaxValue);
            Guard.Against.OutOfRange(remainingStock, nameof(remainingStock), 0, int.MaxValue);
        }
    }
}