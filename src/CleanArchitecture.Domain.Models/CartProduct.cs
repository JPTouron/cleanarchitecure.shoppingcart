﻿using CleanArchitecture.Common.Guards;
using System;

namespace CleanArchitecture.Domain.Models
{
    public class CartProduct : BaseEntity<Guid>
    {
        public CartProduct(Guid id, Guid shoppingCartId, Product product, int quantity) : base(id)
        {
            ValidateInvariants(id, shoppingCartId, product, quantity);

            ShoppingCartId = shoppingCartId;
            Description = product.Description;
            Name = product.Name;
            Price = product.Price;
            Quantity = quantity;
            ProductId = product.Id;
        }

        public string Description { get; }

        public string Name { get; }

        public decimal Price { get; }

        public int ProductId { get; }

        public int Quantity { get; private set; }

        public Guid ShoppingCartId { get; }

        public void UpdateQuantity(int quantity)
        {
            Guard.Against.OutOfRange(quantity, nameof(quantity), 1, int.MaxValue);

            Quantity = quantity;
        }

        private void ValidateInvariants(Guid id, Guid shoppingCartId, Product product, int quantity)
        {
            Guard.Against.EmptyOrNullGuid(id, nameof(id));
            Guard.Against.EmptyOrNullGuid(shoppingCartId, nameof(id));
            Guard.Against.Null(product, nameof(product));
            Guard.Against.OutOfRange(quantity, nameof(quantity), 1, product.RemainingStock);
        }
    }
}