﻿using CleanArchitecture.Common.Guards;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CleanArchitecture.Domain.Models
{
    public class ShoppingCart : BaseEntity<Guid>
    {
        private IList<CartProduct> products;

        private PurchaseOrder purchaseOrder;

        public ShoppingCart(Guid id) : base(id)
        {
            Guard.Against.EmptyOrNullGuid(id, nameof(id));

            products = new List<CartProduct>();
        }

        public bool IsPurchased => purchaseOrder != null;

        public PurchaseOrder PurchaseOrder
        {
            get
            {
                if (!IsPurchased)
                    throw new InvalidOperationException("Cannot get a purchase until the purchase is performed. Please call the PurchaseProductsInCart() method before accessing this property");

                return purchaseOrder;
            }
        }

        public void AddOrUpdateProductInCart(CartProduct product)
        {
            Guard.Against.Null(product, nameof(product));

            var existingProduct = products.FirstOrDefault(x => x.ProductId == product.ProductId);
            if (existingProduct != null)
                products.Remove(existingProduct);

            products.Add(product);
        }

        public IReadOnlyList<CartProduct> GetProductsInCart()
        {
            return (IReadOnlyList<CartProduct>)products;
        }

        public void PurchaseProductsInCart(string customerName)
        {
            purchaseOrder = new PurchaseOrder(customerName);
        }
    }
}