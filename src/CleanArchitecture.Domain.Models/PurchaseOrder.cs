﻿using CleanArchitecture.Common.Guards;
using System;
using System.Collections.Generic;

namespace CleanArchitecture.Domain.Models
{
    public class PurchaseOrder : BaseValueObject<PurchaseOrder>
    {
        public PurchaseOrder(string customerName)
        {
            Guard.Against.NullOrWhiteSpace(customerName, nameof(customerName));

            PurchaseDate = DateTime.Now;
            CustomerName = customerName;
        }

        /// <summary>
        ///  this could be just some customer billing information object, for simplicity we stuck with customer name
        /// </summary>
        public string CustomerName { get; }

        public DateTime PurchaseDate { get; }

        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return CustomerName;
            yield return PurchaseDate;
        }
    }
}